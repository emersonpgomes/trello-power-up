/* global popup:false */
/* global utils:false */

function cardButtons() {
  return [{
    icon: './images/card_logo.svg',
    text: 'GitLab',
    callback: popup.openCard
  }];
}

function cardBadges(t, opts) {
  var attachments = opts.attachments || [];
  var gitlabCount = attachments.filter(function checkAttachment(a) {
    return utils.isUrlGitLab(a.url);
  }).length;

  if (gitlabCount > 0) {
    return [{
      text: gitlabCount.toString(),
      icon: './images/card_logo.svg'
    }];
  }
  return [];
}

function attachmentThumbnail(t, options) {
  // Add gitlab logo to attachment thumbnail
  // for gitlab.com links
  if (utils.isUrlGitLab(options.url)) {
    return {
      url: options.url,
      title: options.name,
      image: {
        url: './images/attachment_logo.svg',
        logo: true
      }
    };
  }

  // Let trello handle the URL instead
  throw t.NotHandled('Not a handled URL');
}

function attachmentSections(t, options) {
  var attachments = options.entries;
  var gitlabAttachments = attachments.filter(utils.isAttachmentGitLabMergeRequest);

  if (gitlabAttachments.length > 0) {
    return [{
      icon: './images/card_logo.svg',
      title: 'GitLab Merge Requests',
      claimed: gitlabAttachments,
      content: {
        type: 'iframe',
        url: t.signUrl('./mr_section.html')
      }
    }];
  }

  return [];
}

TrelloPowerUp.initialize({
  'authorization-status': utils.getAuthorizationStatus,
  'show-authorization': popup.openConfigure,
  'card-buttons': cardButtons,
  'card-badges': cardBadges,
  'attachment-thumbnail': attachmentThumbnail,
  'attachment-sections': attachmentSections
});
