/* global popup:false */
/* global api:false */
/* global utils:false */
/* global project:false */

function attachMergeRequestError(t, error) {
  throw t.NotHandled('error occurred when attaching merge request to card', error);
}

function attachMergeRequest(t, url, name) {
  return t.attach({
    url: url,
    name: name
  })
  .then(popup.close.bind(this, t))
  .catch(attachMergeRequestError.bind(this, t));
}

function getAuth(t, authStatus) {
  if (authStatus && authStatus.authorized) {
    return project.showProjects(t);
  }

  return popup.openConfigure(t);
}

function attach(t) {
  return utils.getAuthorizationStatus(t)
    .then(getAuth.bind(this, t));
}

function getMergeRequestsError(t, error) {
  throw t.NotHandled('error occurred when getting merge requests from GitLab API', error);
}

function getMergeRequests(t, projectId) {
  return utils.getAuthToken(t)
    .then(function getAllMergeRequests(auth) {
      return api.getAllMergeRequests(auth, projectId);
    })
    .catch(getMergeRequestsError.bind(this, t));
}

function mapMergeRequest(t, projectNamespace, mr) {
  var cardTitle = mr.title + ' (!' + mr.iid + ') · ' + projectNamespace;
  return {
    text: '!' + mr.iid + ' ' + mr.title,
    callback: function callbackWrapper(trelloInstance) {
      return attachMergeRequest(trelloInstance, mr.web_url, cardTitle);
    }
  };
}

function showProjectMergeRequestCallback(t, projectNamespace, response) {
  return response.data.map(mapMergeRequest.bind(this, t, projectNamespace));
}

function showProjectMergeRequestError(t, error) {
  throw t.NotHandled('error occurred when getting projects merge requests from GitLab API', error);
}

function showProjectMergeRequest(t, projectId, projectNamespace) {
  return getMergeRequests(t, projectId)
    .then(showProjectMergeRequestCallback.bind(this, t, projectNamespace))
    .then(popup.openMergeRequests.bind(this, t))
    .catch(showProjectMergeRequestError.bind(this, t));
}

window.mergeRequest = {
  attach: attach,
  showProjectMergeRequest: showProjectMergeRequest
};
